export default [
  {
    title: 'Product 1',
    price: 3680,
    image: './img/airpods.jpg'
  },
  {
    title: 'Product 2',
    price: 5100,
    image: './img/bicycle.jpg'
  },
  {
    title: 'Product 3',
    price: 5300,
    image: './img/board.jpg'
  },
  {
    title: 'Product 4',
    price: 3680,
    image: './img/airpods.jpg'
  },
  {
    title: 'Product 5',
    price: 5400,
    image: './img/bicycle.jpg'
  },
  {
    title: 'Product 6',
    price: 5500,
    image: './img/board.jpg'
  },
  {
    title: 'Product 7',
    price: 5416,
    image: './img/airpods.jpg'
  },
  {
    title: 'Product 8',
    price: 5634,
    image: './img/bicycle.jpg'
  },
  {
    title: 'Product 9',
    price: 5123,
    image: './img/board.jpg'
  },
  {
    title: 'Product 10',
    price: 6742,
    image: './img/airpods.jpg'
  },
  {
    title: 'Product 11',
    price: 1233,
    image: './img/bicycle.jpg'
  },
  {
    title: 'Product 12',
    price: 9087,
    image: './img/board.jpg'
  },
  {
    title: 'Product 13',
    price: 6543,
    image: './img/airpods.jpg'
  },
  {
    title: 'Product 14',
    price: 9124,
    image: './img/bicycle.jpg'
  },
  {
    title: 'Product 15',
    price: 2345,
    image: './img/board.jpg'
  },
  {
    title: 'Product 16',
    price: 5653,
    image: './img/bicycle.jpg'
  },
  {
    title: 'Product 17',
    price: 5124,
    image: './img/board.jpg'
  },
  {
    title: 'Product 18',
    price: 53007,
    image: './img/board.jpg'
  }
];
